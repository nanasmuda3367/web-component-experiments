# Web Components Experiments

Random experiments with Web Components API on templates, slots, element extension and interfacing `<form>`.

Goals:

* template-based modularization using slot
* using and accessing slot elements
* CSS behavior in mixing LightDOM and ShadowDOM
* extending existing HTML elements
* interfacing HTML forms
