const newElementTemplate = document.createElement("template");
newElementTemplate.innerHTML = `
	<style type="text/css">
		:root {
			color: blue;
		}
	</style>
	<slot name="attributes"></slot>
`;

class NewElement extends HTMLElement {
	constructor() {
		super();

		let r = this.attachShadow({
			mode: "open"
		});

		r.appendChild(newElementTemplate.content.cloneNode(true));
	}

	connectedCallback() {
		console.log("connected");
	}

	disconnectedCallback() {
		console.log("connected");
	}

	attributeChangedCallback(name, oldValue, newValue) {
		console.log(`attribute ${name} changed: from ${oldValue} to ${newValue}`);
	}
	
	static get observedAttributes() {
		return [];
	}
}

class NewElement2 extends HTMLElement {
	constructor() {
		super();

		this.attachShadow({
			mode: "open"
		});

		this.shadowRoot.innerHTML = `<slot name="attributes"></slot>`;
	}

	connectedCallback() {
		console.log("connected");
	}

	disconnectedCallback() {
		console.log("connected");
	}

	attributeChangedCallback(name, oldValue, newValue) {
		console.log(`attribute ${name} changed: from ${oldValue} to ${newValue}`);
	}

	static get observedAttributes() {
		return [];
	}
}

class NewInput extends HTMLElement {
	constructor() {
		super();

		this.attachShadow({
			mode: "open"
		});

		this.shadowRoot.innerHTML = `<slot name="form-value"></slot>`;

		let slots = this.shadowRoot.querySelectorAll("slot");
		slots.forEach(slot => {
			slot.addEventListener("slotchange", this.slotChangeHandler.bind(this));
		});
	}

	connectedCallback() {
		console.log("connected");
	}

	disconnectedCallback() {
		console.log("connected");
	}

	attributeChangedCallback(name, oldValue, newValue) {
		console.log(`attribute ${name} changed: from ${oldValue} to ${newValue}`);
	}

	static get observedAttributes() {
		return [];
	}

	slotChangeHandler(e) {
		e.target.assignedNodes().forEach(assignedSlot => {
			assignedSlot.querySelectorAll("input").forEach(input => {
				input.value = Math.random().toString()
			});
		});
	}
}

class NewExtendedInput extends HTMLInputElement {
	constructor() {
		super();

		/*
		this.attachShadow({
			mode: "open"
		});
		*/
		console.log(this.innerHTML);
		this.innerHTML = `<b class="label">Label:</b>`;
	}

	connectedCallback() {
		console.log("connected");
	}

	disconnectedCallback() {
		console.log("connected");
	}

	attributeChangedCallback(name, oldValue, newValue) {
		console.log(`attribute ${name} changed: from ${oldValue} to ${newValue}`);
	}

	static get observedAttributes() {
		return [];
	}
}

customElements.define("new-element", NewElement);
customElements.define("new-element2", NewElement2);
customElements.define("new-input", NewInput);
customElements.define("new-extended-input", NewExtendedInput, { extends: "input" });